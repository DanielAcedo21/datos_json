#Relacion Ejercicios JSON Acceso a Datos#
###Por Daniel Acedo###

##Ejercicio 1##
Este ejercicio muestra una lista de ciudades de distintos países y muestra información sobre el tiempo actual en ellas al pulsarlas. La información se recoge de la API en formato JSON de OpenWeatherMap, obteniendo una API Key para su uso anteriormente y pasandole parametros mediante HTTP GET.

![ej1_lista.JPG](https://bitbucket.org/repo/xKjEjq/images/1015571066-ej1_lista.JPG)

![ej1_detalles.JPG](https://bitbucket.org/repo/xKjEjq/images/2207414914-ej1_detalles.JPG)

##Ejercicio 2##
Esta aplicación hace uso de la misma API para obtener el tiempo. Permite poner el nombre de la ciudad y el código del país para su busqueda. En este caso conseguimos la predicción para los siguientes 7 días. Esta información se recoge en formato JSON y se mapea mediante GSON a una clase contenedora. Los datos se guardan en dos ficheros diferentes, uno en JSON y otro en XML, a partir del GSON ya convertido y obtenido.

![ej2_main.JPG](https://bitbucket.org/repo/xKjEjq/images/3191915067-ej2_main.JPG)

![ej2_json.JPG](https://bitbucket.org/repo/xKjEjq/images/3256814628-ej2_json.JPG)

![ej2_xml.JPG](https://bitbucket.org/repo/xKjEjq/images/1938429658-ej2_xml.JPG)

##Ejercicio 3##
Este ejercicio consiste en un convertidor de divisas que se conecta a una API externa (http://api.fixer.io/latest) para obtener los ratios de conversión en formato JSON. Permite seleccionar distintas divisas diferentes. Es prácticamente la misma que la última versión entregada en la última relación.

![ej3.JPG](https://bitbucket.org/repo/xKjEjq/images/20262915-ej3.JPG)

##Ejercicio 4##
Esta aplicación accede a la sección de Datos Abiertos de Málaga para obtener información de las paradas de autobús y mostrarla. La información se obtiene de [aquí](http://datosabiertos.malaga.eu/recursos/transporte/EMT/lineasYHorarios/stops.csv). Al estar la información en CSV he tenido que convertirla a JSON en tiempo de ejecución para parsearlo. Dentro de la información se obtiene el nombre de la parada y una URL a una página con información sobre las líneas que pasan por esa parada y el tiempo real de espera para el siguiente autobus. Para obtener esta información he tenido que obtener el HTML de la página de la parada seleccionada y parsear el HTML con XMLPullParser para extraer la información de las líneas. Al no cumplir el HTML de esta página con los estándares XML de formato he tenido que filtrar las líneas mal formateadas (como etiquetas sin cerrar) para que el parseador no diese error.

En el caso de que no se pueda obtener el tiempo de espera (por ejemplo que la parada no lo tenga integrado o que se consulte en horario con los autobuses fuera de servicio) se mostrará que no hay información. Por lo que se recomienda probar durante el día, cuando los autobuses circulan.

![ej4_lista.JPG](https://bitbucket.org/repo/xKjEjq/images/415801945-ej4_lista.JPG)

![ej4_info.JPG](https://bitbucket.org/repo/xKjEjq/images/3308543557-ej4_info.JPG)